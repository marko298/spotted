'use strict';
var _ = require('lodash');
var Promise = require('bluebird');
var customErrors = require('n-custom-errors');
// var consts = require('../consts');
var roundsSrvc = require('../data-services/rounds');
var gamesSrvc = require('../data-services/games');
var usersSrvc = require('../data-services/users');
var badgeSrvc = require('../data-services/badges');
// var validationUtil = require('../util/validations');
var config = require('../../config/environment');
var log = require('../util/logger').logger;
var apn = require('apn');
// var mongoose = require('mongoose');
var options = config.get("apn-option");
var apnProvider = new apn.Provider(options);

var Ticker = function () {
    this.interval = null;
    this.rounds = [];
};


Ticker.prototype.run = function () {
    if (!this.interval) {
        this.interval = setInterval(this.tick.bind(this), 1000);
    } else {
        throw new Error('Timer is already running');
    }
};

Ticker.prototype.close = function () {
    if (this.interval) {
        clearInterval(this.interval);
    }
};

Ticker.prototype.tick = function () {
    // console.log("ticker ticked");
    // console.log('Timer:', this.rounds);


    this.checkRounds();
};

Ticker.prototype.addRound = function (gameId, roundId, roundEnd) {
    if (typeof roundEnd === 'string') {
        roundEnd = Date.parse(roundEnd);
    }

    if (typeof roundId === 'object') {
        roundId = roundId.toString();
    }

    if (typeof gameId === 'object') {
        gameId = gameId.toString();
    }

    this.rounds.push({
        game: gameId,
        _id:  roundId,
        end:  roundEnd,
    });

    console.log('Timer: added', this.rounds);
};

Ticker.prototype.removeRound = function (roundId) {
    console.log('Timer: before removed', this.rounds);
    if (typeof roundId === 'object') {
        roundId = roundId.toString();
    }

    console.log('Timer: round to remove', roundId, typeof roundId);

    this.rounds = this.rounds.filter((round) => (round._id !== roundId));
    console.log('Timer: after removed', this.rounds);

};

Ticker.prototype.checkRounds = function () {
    var timestamp = +new Date();
    var toDelete = this.rounds.filter((round) => (round.end <= timestamp));
    this.rounds = this.rounds.filter((round) => (round.end > timestamp));

    function notify(round) {
        console.log('Timer: sending push', round);

        function sendPushToJudger(game, judger) {
            var note = new apn.Notification();
            note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
            note.sound = "ping.aiff";
            note.alert = "Timeout.";
            note.payload = {'type': 900, 'data': {'gameId': round.game, 'roundId': round._id}};
            note.topic = "com.kgom.spotted";
            console.log('Timer: pushing', judger);
            if (judger.APNSToken !== undefined) {
                apnProvider
                    .send(note, judger.APNSToken)
                    .then((result) => {
                        log.info('APNS timer', 'result=', result);
                    });
            }

            return game;
        }

        function _sendNotifyJudger(game) {
            return gamesSrvc.getJudgerWithAPNS(game._id).then((judger) => sendPushToJudger(game, judger))
        }


        return gamesSrvc
            .getGame({_id: round.game})
            .then(_sendNotifyJudger)
        // .then(_checkGameStatus)
        // .then((game) => {
        //     roundsSrvc.updateCompleteStatus(game._id);
        // });


    }

    return Promise.all(toDelete.map(notify)).catch(console.log)
};


// function _checkGameStatus(game) {
//     //if all round finished
//     return gamesSrvc.getJudger(game._id)
//                     .then(judger => {
//                         // console.log("-----------judger-----------");
//                         // console.log(judger);
//                         if (judger !== undefined) {
//                             //set judger for game and create a new round with the judger
//                             return gamesSrvc.updateGameJudger(game._id, judger.player)
//                                             .then(result => createRound(game, judger))
//                                             .then(result => sendWriteTaskNotification(game._id, result._id, judger.player))
//                                             .then(result => sendJudgeFinishNotification(game, round._id))
//                                             .then(result => sendUpdateNotifications(game));
//
//                         } else {
//                             //all rounds finished. check who is first. or tie
//
//                             var gamePlayers = game.gamePlayers;
//                             gamePlayers.sort(function (a, b) {
//                                 return a.point < b.point
//                             });
//                             console.log("-----------gamePlayers-----------");
//                             //console.log(gamePlayers);
//                             var top = [];
//                             var topPoint = gamePlayers[0].point;
//                             for (var i = 0; i < gamePlayers.length; i++) {
//                                 if (gamePlayers[i].point === topPoint)
//                                     top.push(gamePlayers[i]);
//                             }
//                             if (top.length === 1) {
//                                 //complete game.
//                                 console.log("-----------complete-----------");
//                                 //console.log(gamePlayers);
//                                 var winner = top[0].player;
//                                 gamesSrvc.updateCompleteStatus(game._id, top[0].player)
//                                          .then(result => increaseGamesWon(game))
//                                          .then(result => increaseGamesPlayed(game))
//                                          .then(result => presentBadge(game))
//                                          .then(result => sendJudgeFinishNotification(game))
//                                          .then(result => sendGameCompleteNotification(game, winner))
//                                          .then(result => sendUpdateNotifications(game));
//                             } else if (top.length === gamePlayers.length) {
//                                 console.log("-----------all tie-----------");
//                                 var lastPlayer = gamePlayers[gamePlayers.length - 1];
//                                 updateGamePlayersNotJudged(game._id, gamePlayers);
//                                 gamesSrvc.updateGameJudger(game._id, lastPlayer.player)
//                                          .then(result => createRound(game))
//                                          .then(result => {
//                                              return sendWriteTaskNotification(game._id, result._id, lastPlayer);
//                                          })
//                                          .then(result => sendJudgeFinishNotification(game))
//                                          .then(result => sendUpdateNotifications(game));
//
//                             } else {
//                                 //tie
//                                 console.log("-----------tie-----------");
//                                 //console.log(gamePlayers);
//                                 var lastPlayer = gamePlayers[gamePlayers.length - 1];
//                                 for (var j = top.length; j < gamePlayers.length; j++) {
//                                     gamesSrvc.updateGamePlayerState(game._id, gamePlayers[j].player, "losed");
//                                 }
//                                 gamesSrvc.updateGameJudger(game._id, lastPlayer.player)
//                                          .then(result => createRound(game))
//                                          .then(result => sendWriteTaskNotification(game._id, result._id, lastPlayer))
//                                          .then(result => sendJudgeFinishNotification(game))
//                                          .then(result => sendUpdateNotifications(game));
//                             }
//                             return Promise.resolve(data);
//                         }
//                     });
//
// }
//
// function createRound(game, judger) {
//     //var startTime = (new Date).getTime();
//     //var endTime = startTime + config.get("time").round;
//     var startTime = 0;
//     var endTime = 0;
//     var roundData = {
//         roundName:  "Round 1",
//         roundIndex: game.currentRound.roundIndex + 1,
//         judger:     judger.player,
//         status:     "open",
//         startTime:  startTime,
//         endTime:    endTime,
//         _gameId:    game._id
//     };
//     return roundsSrvc.createRound(roundData)
//                      .then(round => {
//                          gamesSrvc.addRoundById(game._id, round._id);
//                          return round;
//                      })
//                      .then(round => {
//                          var gamePlayers = game.gamePlayers;
//                          for (var i = 0; i < gamePlayers.length; i++) {
//                              var playerData = gamePlayers[i];
//                              gamesSrvc.updatePlayerAnswered(game._id, playerData.player, false);
//                          }
//
//                          return round;
//                      });
// }
//
// function sendWriteTaskNotification(gameId, roundId, judger) {
//     var note = new apn.Notification();
//     note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
//     note.sound = "ping.aiff";
//     note.alert = "Please write task.";
//     note.payload = {'type': 800, 'data': {'gameId': gameId, 'roundId': roundId}};
//     note.topic = "com.kgom.spotted";
//     if (judger.APNSToken !== undefined) {
//         //console.log(inviter.APNSToken);
//         apnProvider.send(note, judger.APNSToken)
//                    .then((result) => {
//                        log.info('APNS', 'result=', result);
//                    });
//     }
//     return Promise.resolve(judger);
// }
//
// function updateGamePlayersNotJudged(gameId, gamePlayers) {
//     for (var i = 0; i < gamePlayers.length; i++) {
//         var player = gamePlayers[i].player;
//         gamesSrvc.updatePlayerJudged(gameId, player, false);
//     }
// }
//
// function sendJudgeFinishNotification(game, roundId) {
//     console.log("-----------Judge Finish-------");
//     var note = new apn.Notification();
//     note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
//     note.alert = "Time is out";
//     note.sound = "ping.aiff";
//     note.payload = {'type': 900, 'data': {'gameId': game._id, 'roundId': roundId}};
//     note.topic = "com.kgom.spotted";
//     var players = game.gamePlayers;
//     for (var i = 0; i < players.length; i++) {
//         var player = players[i].player;
//         //console.log(inviter);
//         if (player.APNSToken !== undefined && !player._id.equals(game.userId)) {
//             //console.log(inviter.APNSToken);
//             apnProvider.send(note, player.APNSToken)
//                        .then((result) => {
//                            log.info('APNS', 'result=', result);
//                        });
//         }
//
//     }
//     return Promise.resolve(game);
// }
//
// function sendUpdateNotifications(game) {
//     console.log("-----------sendUpdateNotifications-------");
//     if (!game.currentRound) {
//         return customErrors.rejectWithObjectNotFoundError('currentRound is empty.');
//     }
//     var note = new apn.Notification();
//     note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
// //    note.sound = "ping.aiff";
//     note.payload = {'type': 400, 'data': {}};
//     note.topic = "com.kgom.spotted";
//     var players = game.gamePlayers;
//     for (var i = 0; i < players.length; i++) {
//         var player = players[i].player;
//         //console.log(inviter);
//         if (player.APNSToken !== undefined) {
//             //console.log(inviter.APNSToken);
//             apnProvider.send(note, player.APNSToken)
//                        .then((result) => {
//                            log.info('APNS', 'result=', result);
//                        });
//         }
//
//     }
//     return Promise.resolve(game);
// }
//
// function increaseGamesWon(game, winner) {
//     if (winner) {
//         return usersSrvc.increaseGamesWon(winner._id, 1)
//                         .then(result => gamesSrvc.updateGamePlayerState(game._id, winner._id, "won"));
//     } else {
//         return Promise.resolve(data);
//     }
//
// }
//
// function increaseGamesPlayed(game) {
//     var gamePlayers = game.gamePlayers;
//     var players = [];
//     for (var i = 0; i < gamePlayers.length; i++) {
//         let player = gamePlayers[i].player;
//         players.push(player._id);
//
//
//     }
//     return usersSrvc.increaseGamesPlayed(players, 1)
//                     .then(result => {
//                         return Promise.resolve(game);
//                     })
//
// }
//
// function presentBadge(game) {
//     return gamesSrvc.getGamePlayers(game._id)
//                     .then(gamePlayers => {
//                         console.log("---------------presentBadge--------------");
//                         for (var i = 0; i < gamePlayers.length; i++) {
//                             presentBadgeForPlayer(game._id, gamePlayers[i]);
//
//                         }
//                         return Promise.resolve(game);
//                     })
//
// }
//
// function sendGameCompleteNotification(game, winner) {
//     console.log("-----------CompleteNotification-------");
//     if (!data.winner) {
//         return Promise.resolve(game);
//     }
//     var note = new apn.Notification();
//     note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
//     note.alert = "You won the Game.";
//     note.sound = "ping.aiff";
//     note.payload = {'type': 500, 'data': {'gameId': game._id}};
//     note.topic = "com.kgom.spotted";
//     apnProvider.send(note, winner.APNSToken)
//                .then((result) => {
//                    log.info('APNS', 'result=', result);
//                });
//     return Promise.resolve(game);
//
// }
//
// function presentBadgeForPlayer(gameId, gamePlayer) {
//     console.log("---------------presentBadgeForPlayer--------------");
//     //  console.log(gamePlayer);
//     var player = gamePlayer.player;
//     if (player.badges.length > 0) {
//         var filter = {
//             "_id": {$nin: player.badges}
//         };
//     } else {
//         var filter = {};
//     }
//     badgeSrvc.getBadges(filter, '_id badgeName formular imageUrl description')
//              .then(badges => {
//                  for (var i = 0; i < badges.length; i++) {
//                      let badge = badges[i];
//                      console.log(player.gamesPlayed + " " + player.gamesWon);
//                      var available = checkBadgeForPlayer(badge, player.gamesPlayed, player.gamesWon, player.point);
//                      if (available) {
//
//                          usersSrvc.addBadge(player._id, badge);
//                          gamesSrvc.addBadgeToGamePlayer(gameId, player._id, badge._id);
//                      }
//                  }
//              });
// }
//
// function checkBadgeForPlayer(badge, gamesPlayed, gamesWon, point) {
//     console.log("---------------check badge-------------");
//     var allowedFields = ['property', 'relation', 'value'];
//     // console.log(badge.formular);
//     try {
//         var formular = JSON.parse(badge.formular)
//     } catch (e) {
//         var formular = {};
//         console.error("Invalid json");
//     }
//
//     var value1 = formular.value;
//     var value2 = gamesPlayed;
//     if (formular.property === "gamesPlayed") {
//         value2 = gamesPlayed;
//     } else if (formular.property === "gamesWon") {
//         value2 = gamesWon;
//     } else if (formular.property === "point") {
//         value2 = point;
//     }
//
//     if (formular.relation === "equal") {
//         return value1 === value2;
//     } else if (formular.relation === "smaller") {
//         return value1 >= value2;
//     } else {
//         return value1 <= value2;
//     }
//     return false;
//
// }
//

module.exports = new Ticker();
